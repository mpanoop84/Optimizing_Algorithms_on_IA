//=======================================================================================
//
// SAMPLE SOURCE CODE - SUBJECT TO THE TERMS OF SAMPLE CODE LICENSE AGREEMENT,
// http://software.intel.com/en-us/articles/intel-sample-source-code-license-agreement/
//
// Copyright 2017 Intel Corporation
//
// THIS FILE IS PROVIDED "AS IS" WITH NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT
// NOT LIMITED TO ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE, NON-INFRINGEMENT OF INTELLECTUAL PROPERTY RIGHTS.
//
// ======================================================================================
#include<iostream>
#include<opencv2/opencv.hpp>
#include<math.h>
#include<chrono>
#define PI 3.14159265f
using namespace cv;
using namespace std;

void convertRGBtoGray(Mat &output, Mat &input)
{
	int rows = input.rows;
	int cols = input.cols;
	for(int i = 0; i < rows*cols; i++)
		output.data[i] = input.data[i*3];
	return;
}

void GaussianFilter(Mat &output, Mat &input)
{
	float filter[3][3] = {{0.04491, 0.12210, 0.04491}, {0.12210, 0.33191, 0.12210}, {0.04491, 0.12210, 0.04491}};
	int rows = output.rows;
	int cols = output.cols;
	float value;
	for(int i = 1; i < rows; i++)
	{
		int index = i*cols+1;
		for(int j = index; j < index+cols; j++)
		{
			value = 0.0f;
			for(int k1 = -1; k1 <= 1; k1++)
			{
				int index1 = j+(k1*cols);
				for(int k2 = -1; k2 <= 1; k2++)
					value += filter[k1+1][k2+1]*input.data[j+k2];
			}
			output.data[j] = value; 
		}
	}
	return;
}
	
void computeGradient(Mat &input, Mat *strength, Mat *direction)
{
	float gradientx[3][3] = {{-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1}}; 
	float gradienty[3][3] = {{-1, -2, -1}, {0, 0, 0}, {1, 2, 1}};
	int rows = input.rows;
	int cols = input.cols;
	int gcols = (*strength).cols;
	float gradient_x, gradient_y; 
	float degrees; 
	for(int i = 1; i < rows-1; i++)
	{
		int index = i*cols+1;
		int gindex = (i-1)*gcols;
		#pragma omp simd private(gradient_x, gradient_y, degrees)
		for(int j = 0; j < cols-2; j++)
		{
			int index1 = index+j;
			int gindex1 = gindex+j;
			gradient_x = gradient_y = 0.0f;
			for(int k1 = -1; k1 <= 1; k1++)
			{
				int index2 = index1+(k1*cols);
				for(int k2 = -1; k2 <=1; k2++)
				{
					gradient_x += gradientx[k1+1][k2+1] * input.data[index2+k2];
					gradient_y += gradienty[k1+1][k2+1] * input.data[index2+k2];
				}
			}
			(*strength).data[gindex1] = sqrtf(powf(gradient_x, 2.f) + powf(gradient_y, 2.f));
			degrees = floorf(((atan2(gradient_y, gradient_x) * 180.f / PI)/45.f) + 0.5f); 		
			(*direction).data[gindex1] = (degrees == 4)?0:(degrees*45);
		}
	}
	return;
}

void nonMaximumSupression(Mat &strength, Mat &direction)
{
	int row = strength.rows;
	int col = strength.cols;
	for(int i = 0; i < row; i++)
	{
		int index = i*col;
		for(int j = index; j < index+col; j++)
		{
			int index1 = j-col;
			int index2 = j+col;
			switch(direction.data[j])
			{
				case 0: if(!(strength.data[j]>strength.data[index1] && strength.data[j]>strength.data[index2]))
					strength.data[j] = 0;
					break;
				case 45: if(!(strength.data[j]>strength.data[index1-1] && strength.data[j]>strength.data[index2+1]))
					strength.data[j] = 0;
					break;
				case 90: if(!(strength.data[j]>strength.data[index-1] && strength.data[j]>strength.data[index+1]))
					strength.data[j] = 0;
					break;
				case 135: if(!(strength.data[j]>strength.data[index1+1] && strength.data[j]>strength.data[index2-1]))
					strength.data[j] = 0;
					break;
			}	
			
		}
	}
}

int main()
{
	Mat inputimage;
	inputimage = imread("../res/silverfalls.bmp", CV_LOAD_IMAGE_COLOR);
	int rows = inputimage.rows;
	int cols = inputimage.cols;
        int newrows = rows+2;
        int newcols = cols+2;
	Mat outputimage(rows, cols, CV_8UC1);
	Mat gradientstrength(rows, cols, CV_8UC1);
	Mat gradientdirection(rows, cols, CV_8UC1);
	memset(gradientstrength.data, 0, rows*cols);
	memset(gradientdirection.data, 0, rows*cols);
	Mat outputimage1(newrows, newcols, CV_8UC1);
	Mat outputimage2(newrows, newcols, CV_8UC1);	
        memset(outputimage1.data, 0, rows*cols);
        memset(outputimage2.data, 0, rows*cols);
	std::chrono::time_point<std::chrono::system_clock> timer_start, timer_stop, timer_start1, timer_stop1, timer_start2, timer_stop2, timer_start3, timer_stop3, timer_start4, timer_stop4;
	if(!inputimage.data)
	{
		cout<<"failed to open the image\n";
		return -1;
	}
	timer_start = std::chrono::system_clock::now();
	//Step 1 RGB to Gray Scale Conversion
	timer_start1 = std::chrono::system_clock::now();
	convertRGBtoGray(outputimage, inputimage);
	timer_stop1 = std::chrono::system_clock::now();
	//Get padding around the image for applying Gaussian and other filters
	for(int i = 0; i < rows; i++)
	{
		int index1 = (i+1)*newcols+1;
		int index = i*cols;
		for(int j = 0; j < cols; j++)
			outputimage1.data[index1+j] = outputimage.data[index+j];
	}
	//Step 2 Gaussian Filter on Gray Scale Image
	timer_start2 = std::chrono::system_clock::now();
	GaussianFilter(outputimage2, outputimage1); 
	timer_stop2 = std::chrono::system_clock::now();

        for(int i = 0; i < rows; i++)
        {
                int index1 = (i+1)*newcols+1;
                int index = i*cols;
                for(int j = 0; j < cols; j++)
                        outputimage.data[index+j] = outputimage2.data[index1+j];
        }
	//Step 3 Gradient strength and direction
	timer_start3 = std::chrono::system_clock::now();
	computeGradient(outputimage2, &gradientstrength, &gradientdirection);
	timer_stop3 = std::chrono::system_clock::now();
	//Step 4 Non-maximum supression
	timer_start4 = std::chrono::system_clock::now();
	nonMaximumSupression(gradientstrength, gradientdirection);
	timer_stop4 = std::chrono::system_clock::now();
	timer_stop = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = timer_stop - timer_start;
	std::chrono::duration<double> elapsed_seconds1 = timer_stop1 - timer_start1;
	std::chrono::duration<double> elapsed_seconds2 = timer_stop2 - timer_start2;
	std::chrono::duration<double> elapsed_seconds3 = timer_stop3 - timer_start3;
	std::chrono::duration<double> elapsed_seconds4 = timer_stop4 - timer_start4;
	std::cout<<"Time elapsed for 1 frame = "<<elapsed_seconds.count()<<" seconds\n";
	std::cout<<"Time taken by RGB to GrayScale = "<<elapsed_seconds1.count()<<" seconds\n";
	std::cout<<"Time taken by Gaussian Filter = "<<elapsed_seconds2.count()<<" seconds\n";
	std::cout<<"Time taken by computeGradient = "<<elapsed_seconds3.count()<<" seconds\n";
	std::cout<<"Time taken by Non-Maximum Suppression = "<<elapsed_seconds4.count()<<" seconds\n";
	namedWindow("NMS Image", CV_WINDOW_NORMAL);
	imshow("NMS Image", gradientstrength);
        namedWindow("Input Image", CV_WINDOW_NORMAL); 
        imshow("Input Image", inputimage);

	waitKey(0);
	return 0;
}
